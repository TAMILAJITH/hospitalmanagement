import { DoctorVM, GridColumn, PatientVM } from '../Interfaces/Interfaces';

export class DoctorGridInstance {

    GridColumns: GridColumn[] = [];
    GridData= []
}
export class PatientGridInstance {

    GridColumns: GridColumn[] = [];
    GridData: PatientVM[] = [];

}

