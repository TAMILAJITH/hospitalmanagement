import { trigger, animate, query, style, transition, group } from '@angular/animations';

export const fader =
    trigger('routeAnimation', [

        transition('1 => 2', [
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    left: 0,
                    width: '100%',
                    opacity: 0,
                    transform: 'scale(0) translateX(100%)'
                })
            ]),
            query(':enter', [
                animate('900ms ease', style({ opacity: 1, transform: 'scale(1) translateX(0)' }))
            ])
        ]),
        transition('2 => 1', [
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    width: '100%',
                    opacity: 0,
                    transform: 'scale(0) translateY(0)'
                })
            ]),
            query(':enter', [
                animate('600ms ease', style({ opacity: 1, transform: 'scale(1) translateY(100%)' }))
            ])
        ])
    ]);

export const slider = trigger('routeAnimation', [
    transition('1 => 2', [
        group([
            query(':enter', style({ position: 'absolute', left: 0, right: 0, bottom: 0,top:0, transform: 'translatex(100%)' }))
        ]),
        group([
            query(':leave', [animate('600ms', style({ transform: 'translateX(-100%)' }))]),
            query(':enter', [animate('600ms', style({ transform: 'translateX(0)' }))])
        ])  
    ]),
    transition('2 => 1', [
        group([
            query(':enter', style({ position: 'absolute', left: 0, right: 0, bottom: 0,top:0, transform: 'translateX(-100%)' }))
        ]),
        group([
            query(':leave', [animate('600ms', style({ transform: 'translateX(100%)' }))]),
            query(':enter', [animate('600ms', style({ transform: 'translateX(0)' }))])
        ])
    ])
])
