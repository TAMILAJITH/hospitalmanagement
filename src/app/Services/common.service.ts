import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BookingCVM, BookingUVM, BookingVM, DoctorCVM, DoctorVM, GridColumn, PatientCVM, PatientVM, Url } from '../Interfaces/Interfaces';
import { BaseUrl } from '../../assets/Base/BaseUrl';
import { DoctorGridInstance } from '../Instance/GridInstance';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient, private router: Router) { }

  public url: Url;
  public IsDoctorActive: boolean;
  public IsPatientActive: boolean;
  public IsBookingActive: boolean = false;
  public IsAddBookingActive: boolean;
  public IsAppointmentActive: boolean;
  public doctorData: DoctorVM[];
  public patientData: PatientVM[];
  public doctorDetail: DoctorVM;
  public patientDetail: PatientVM;
  public detailHeaderName: string;
  public currentUserDetail: any = {};
  public activateDoctor: boolean;
  public activatePatient: boolean;
  public getBookingData: BookingVM[];
  public showBookingData: BookingVM[];
  public patientRecIdToBooking: string;
  public bookingDetail: BookingVM;
  public fromBookingDetail: boolean = false;



  public grid = new DoctorGridInstance();
  public doctorGridColumn: GridColumn[] = [
    { Name: "RecId", Definition: "RecId" },
    { Name: "Name", Definition: "Name" },
    { Name: "Qualification", Definition: "Qualification" },
    { Name: "Email", Definition: "Email" },
    { Name: "PhoneNumber", Definition: "PhoneNumber" },
    { Name: "Action", Definition: "Action" }
  ];
  public patientGridColumn: GridColumn[] = [
    { Name: "RecId", Definition: "RecId" },
    { Name: "Name", Definition: "Name" },
    { Name: "Email", Definition: "Email" },
    { Name: "PhoneNumber", Definition: "PhoneNumber" },
    { Name: "Action", Definition: "Action" }

  ];
  public bookingGridColumn: GridColumn[] = [
    { Name: "RecId", Definition: "RecId" },
    { Name: "Doctor", Definition: "Doctor" },
    { Name: "Patient", Definition: "Patient" },
    { Name: "BookingTime", Definition: "BookingTime" },
    { Name: "BookingDate", Definition: "BookingDate" },
    { Name: "Description", Definition: "Description" },
    { Name: "Action", Definition: "Action" }

  ]

  activeDoctorTab() {
    debugger
    this.IsDoctorActive = true;
    this.IsPatientActive = false;
    this.IsBookingActive = false;
    this.IsAddBookingActive = false;
    this.IsAppointmentActive = false;
  }

  activePatientTab() {
    this.IsDoctorActive = false;
    this.IsPatientActive = true;
    this.IsBookingActive = false;
    this.IsAddBookingActive = false;
    this.IsAppointmentActive = false;

  }

  activeBookingTab() {
    this.IsDoctorActive = false;
    this.IsPatientActive = false;
    this.IsBookingActive = true;
    this.IsAddBookingActive = false;
  }

  activeAddBookingTab() {
    this.IsDoctorActive = false;
    this.IsPatientActive = false;
    this.IsBookingActive = false;
    this.IsAddBookingActive = true;
  }

  activeAppointmentMenu() {
    this.IsAppointmentActive = true;
    this.IsDoctorActive = false;
    this.IsPatientActive = false;
    this.IsBookingActive = false;
    this.IsAddBookingActive = false;
  }


  getDoctorGridData() {
    this.getDoctorData().subscribe((x: any) => {
      debugger
      this.grid.GridData = x;
      this.doctorData = x;

    });
  }

  getPatientGridData() {
    this.getPatientData().subscribe((x: any) => {
      this.grid.GridData = x;
      this.patientData = x;

    });
  }

  getBookingGridData() {
    this.getBookingsData().subscribe((x: any) => {
      this.getBookingData = x;
      this.showBookingData = x;
      this.changeDocAndPatRecIdIntoNameAndGetBookingData();
    });
  }

  setGridData() {

    if (this.IsDoctorActive) {
      this.grid.GridColumns = this.doctorGridColumn;
      this.getDoctorGridData();
      this.routeToGridPage();
    } else if (this.IsPatientActive) {
      this.grid.GridColumns = this.patientGridColumn;
      this.getPatientGridData();
      this.routeToGridPage();
    } else if (this.IsBookingActive || this.IsAppointmentActive) {
      this.grid.GridColumns = this.bookingGridColumn;
    }
  }

  routeToGridPage() {
    this.router.navigate(["/home/grid"]);

  }

  changeDocAndPatRecIdIntoNameAndGetBookingData() {
    debugger
    if (this.IsBookingActive) {
      this.getBookingData.map((x, i) => {
        debugger
        let getDoctorName = this.doctorData.filter((y) => {
          return y.RecId == x.DoctorRecId
        });
        let getPatientName = this.patientData.filter((z) => {
          return z.RecId == x.PatientRecId
        });
        this.showBookingData[i].DoctorRecId = getDoctorName[0].Name;
        this.showBookingData[i].PatientRecId = getPatientName[0].Name;
        this.showBookingData[i].BookingTime = x.BookingTime.slice(11, x.BookingTime.length - 3);
        this.showBookingData[i].BookingDate = x.BookingDate.slice(0, 10);
      });
    } else {
      let currentDoctorRecord = this.getBookingData.filter((x) => {
        return x.DoctorRecId == this.currentUserDetail.RecId
      });
      currentDoctorRecord.map((x, i) => {
        let getDoctorName = this.doctorData.filter((y) => {
          return y.RecId == x.DoctorRecId
        });
        let getPatientName = this.patientData.filter((z) => {
          return z.RecId == x.PatientRecId
        });
        currentDoctorRecord[i].PatientRecId = getPatientName[0].Name;
        currentDoctorRecord[i].DoctorRecId = getDoctorName[0].Name;
        currentDoctorRecord[i].BookingDate = x.BookingDate.slice(0, 10);
        currentDoctorRecord[i].BookingTime = x.BookingTime.slice(11, x.BookingTime.length - 3);
        this.showBookingData = currentDoctorRecord;
      })
    }
    this.setGridData();
    this.routeToGridPage();

  }

  setChangedBookingDataToGrid() {

    this.getDoctorGridData();
    this.getPatientGridData();
    this.getBookingGridData();
  }

  getUrlFromJson() {
    return this.http.get("assets/Json/BaseUrl.json");
  }

  setDetailHeader() {
    if (this.IsDoctorActive) { this.detailHeaderName = "Doctor" }
    else this.detailHeaderName = "Patient";
  }

  getDoctorData() {
    debugger
    let url = this.url.BaseUrl + "doctors";
    return this.http.get<DoctorVM>(url);
  }

  postDoctorData(DoctorData: DoctorCVM) {
    let url = this.url.BaseUrl + "doctors/create";
    return this.http.post(url, DoctorData);
  }

  postPatientData(patientData: PatientCVM) {
    let url = this.url.BaseUrl + "patients/create";
    return this.http.post(url, patientData);
  }

  getPatientData() {
    let url = this.url.BaseUrl + "patients";
    return this.http.get<DoctorVM>(url);
  }

  getSingleDoctorData(RecId: string) {
    let url = this.url.BaseUrl + "doctors/" + RecId;
    return this.http.get<DoctorVM>(url);
  }
  getSinglePatientData(RecId: string) {
    let url = this.url.BaseUrl + "patients/" + RecId;
    return this.http.get<PatientVM>(url);
  }

  getBookingsData() {
    let url = this.url.BaseUrl + "bookings";
    return this.http.get<BookingVM>(url);
  }

  postBookingData(bookingData: BookingCVM) {
    let url = this.url.BaseUrl + "bookings/create";
    return this.http.post(url, bookingData);
  }

  putBookingData(RecId: string, bookingData: BookingCVM) {
    let url = this.url.BaseUrl + "bookings/update/" + RecId;
    return this.http.put(url, bookingData);
  }

  deleteBookingData(RecId: string) {
    let url = this.url.BaseUrl + "booking/delete?RecId=" + RecId;
    return this.http.delete(url);
  }

}
