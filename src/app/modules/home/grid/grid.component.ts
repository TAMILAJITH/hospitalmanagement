import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorGridInstance } from 'src/app/Instance/GridInstance';
import { DoctorVM, GridColumn } from 'src/app/Interfaces/Interfaces';
import { CommonService } from 'src/app/Services/common.service';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  constructor(public service: CommonService, private router: Router) { }

  ngOnInit(): void {

  }

  navigateToDetailPage(data) {
    if (this.service.IsDoctorActive) {
      this.service.doctorDetail = data;
      this.service.setDetailHeader();

    } else if (this.service.IsPatientActive) {
      this.service.patientDetail = data;
      this.service.setDetailHeader();
    } else {
      this.service.bookingDetail = data;
    }
    this.router.navigate(["/home/detail"]);
  }



}
