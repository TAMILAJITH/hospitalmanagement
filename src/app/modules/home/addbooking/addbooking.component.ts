import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/Services/common.service';

@Component({
  selector: 'app-addbooking',
  templateUrl: './addbooking.component.html',
  styleUrls: ['./addbooking.component.css']
})
export class AddbookingComponent implements OnInit, OnDestroy {

  constructor(private fb: FormBuilder, public service: CommonService, private router: Router) { }

  ngOnInit(): void {

  }

  Form: FormGroup = this.fb.group({
    DoctorRecId: ["", Validators.required],
    PatientRecId: [this.service.patientDetail.RecId ? this.service.patientDetail.RecId : ""],
    BookingDate: [this.service.fromBookingDetail ? this.service.bookingDetail.BookingDate : "", Validators.required],
    BookingTime: [this.service.fromBookingDetail ? this.service.bookingDetail.BookingTime : "", Validators.required],
    Description: [this.service.fromBookingDetail ? this.service.bookingDetail.Description : "", Validators.required]
  })

  public submitted = false;

  get doctor() {
    return this.Form.get("DoctorRecId")
  }
  get bookingDate() {
    return this.Form.get("BookingDate")
  }
  get bookingTime() {
    return this.Form.get("BookingTime")
  }
  get description() {
    return this.Form.get("Description")
  }

  onSubmit() {
    debugger
    this.submitted = true;
    if (this.Form.invalid) {
      this.Form.reset();
      alert("please given value to all fields");
      return;
    }
    let formValue = this.Form.value;

    if (!this.service.fromBookingDetail) {
      if (Object.keys(formValue).length > 0 && Object.keys(formValue) != null) {
        this.service.postBookingData(formValue).subscribe(() => {
          alert("Booking Added");
          this.service.activeBookingTab();
          this.service.setChangedBookingDataToGrid();
          this.router.navigate(["/home/grid"]);
        });
      }
    } else {
      if (Object.keys(formValue).length > 0 && Object.keys(formValue) != null) {
        this.service.putBookingData(this.service.bookingDetail.RecId, formValue).subscribe(() => {
          alert("Booking updated");
          this.service.activeBookingTab();
          this.service.setChangedBookingDataToGrid();
          this.service.fromBookingDetail = false;
          this.router.navigate(["/home/grid"]);
        });
      }
    }
  }


  ngOnDestroy() {
    this.service.fromBookingDetail = false;
  }






}









