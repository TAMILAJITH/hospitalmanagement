import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/Services/common.service';
import { GridComponent } from '../grid/grid.component';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router, public service: CommonService) { }

  ngOnInit(): void { }

  logout() {
    if (!confirm("Are you sure")) return;
    this.service.IsDoctorActive = false;
    this.service.IsPatientActive = false;
    this.service.activateDoctor = false;
    this.service.activatePatient = false;
    this.service.IsAddBookingActive = false;
    this.service.IsBookingActive = false;
    localStorage.removeItem("currentUserData");
    localStorage.removeItem("currentUserIdentity");

    this.router.navigate(["/login"]);
  }

  getUserIdentity() {
    debugger
    let currentUserIdentity = JSON.parse(localStorage.getItem("currentUserIdentity" || "''"));
    if (currentUserIdentity.length > 0) {
      if (currentUserIdentity == "Doctor") {
        this.service.IsDoctorActive = true;
        this.service.IsPatientActive = false;
        this.service.IsAppointmentActive = false;
      }
      else {
        this.service.IsPatientActive = true;
        this.service.IsDoctorActive = false;
        this.service.IsBookingActive = false;
        this.service.IsAddBookingActive = false;

      }
    }
  }

  getUserDetail() {
    debugger
    this.getUserIdentity();
    if (this.service.IsDoctorActive) {
      this.service.doctorDetail = this.service.currentUserDetail;
      this.service.setDetailHeader();
    } else {
      this.service.patientDetail = this.service.currentUserDetail;
      this.service.setDetailHeader();
    }
    this.router.navigate(["home/detail"]);
  }

  getDoctorGridData() {
    debugger
    this.service.activeDoctorTab();
    this.service.setGridData();
  }

  getPatientGridData() {
    debugger
    this.service.activePatientTab();
    this.service.setGridData();
  }

  getBookingData() {
    debugger
    this.service.activeBookingTab();
    this.service.setChangedBookingDataToGrid();
    // this.service.routeToGridPage();

  }

  addBooking() {
    this.service.activeAddBookingTab();
    this.router.navigate(["/home/addbooking"]);
  }

  getAppointmentList() {

    this.service.activeAppointmentMenu();
    this.service.setChangedBookingDataToGrid();

  }

}
