import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { GridComponent } from './grid/grid.component';
import { HomeComponent } from './home/home.component';
import {AddbookingComponent} from './addbooking/addbooking.component';

const routes: Routes = [
  {
    path: "", component: HomeComponent, children: [
      { path: "grid", component: GridComponent },
      { path: "detail", component: DetailComponent },
      {path:"addbooking",component:AddbookingComponent}
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
