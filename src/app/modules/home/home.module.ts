import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home/home.component';
import {GridComponent} from './grid/grid.component';
import {NavbarComponent} from './navbar/navbar.component';
import { AppComponent } from 'src/app/app.component';
import { DetailComponent } from './detail/detail.component';
import { AddbookingComponent } from './addbooking/addbooking.component';
import {ReactiveFormsModule,FormsModule} from "@angular/forms";


@NgModule({
    declarations: [
        HomeComponent,
        GridComponent,
        NavbarComponent,
        DetailComponent,
        AddbookingComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        FormsModule,
        ReactiveFormsModule
        

    ],
    providers: [],
    bootstrap:[AppComponent]
})
export class HomeModule { }