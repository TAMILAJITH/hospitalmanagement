import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/Services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service: CommonService) { }

  ngOnInit(): void {
    this.service.getUrlFromJson().subscribe((x:any) => { this.service.url = x });
    this.service.getDoctorData().subscribe((x: any) => { this.service.doctorData = x; });
  }

}
