import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/Services/common.service';
import {transition,} from '@angular/animations';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(public service: CommonService, private router: Router) { }

  ngOnInit(): void {
  }

  passData() {
    this.service.fromBookingDetail = true;
    this.service.IsBookingActive = false;
    this.router.navigate(["/home/addbooking"]);
  }

  deleteBookingData() {
    debugger
    if (!confirm("Are you sure")) return;
    this.service.deleteBookingData(this.service.bookingDetail.RecId).subscribe(() => {
      debugger
      alert("deleted successfully");
      this.service.activeBookingTab();
      this.service.setChangedBookingDataToGrid();
      this.service.routeToGridPage();
    });
  }

  backToList() {
    if (this.service.IsDoctorActive) {
      this.service.setGridData();
    } else if (this.service.IsPatientActive) {
      this.service.setGridData();
    } else {
      this.service.setChangedBookingDataToGrid();
    }
  }
}














