export class DoctorCVM {
    Name: string
    Qualification: String
    Email: String
    PhoneNumber: number
    ProfileImage: String
    UserName: String
    Password: number
}

export class DoctorVM extends DoctorCVM {
    RecId: string
    CreatedBy: String
    CreatedTime: Date
    DataAreaId: String
}

export class PatientCVM {

    Name: string
    Email: String
    PhoneNumber: number
    ProfileImage: String
    UserName: String
    Password: number
}

export class PatientVM extends PatientCVM {
    RecId: string
    CreatedBy: String
    CreatedTime: Date
    DataAreaId: String
}

export class GridColumn {
    Name: string
    Definition: string
}

export class BookingCVM {
    DoctorRecId: string
    PatientRecId: string
    BookingTime: string
    BookingDate: string
    Description: string
}
export class BookingUVM extends BookingCVM {
    RecId: string
}
export class BookingVM extends BookingUVM {
    CreatedBy: string
    CreatedTime: Date
    DataAreaId: string
}
export class Url {
    BaseUrl: string
}








