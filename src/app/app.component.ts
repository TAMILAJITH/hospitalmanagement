import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Url } from './Interfaces/Interfaces';
import { CommonService } from './Services/common.service';
import { fader, slider } from './route-animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // animations: [fader]
  animations: [ slider ]
})

export class AppComponent implements OnInit {
  title = 'HospitalManagement';
  public constructor(private service: CommonService, private router: Router) {

  }

  ngOnInit() {
    debugger
    this.service.getUrlFromJson().subscribe((x: any) => { this.service.url = x; });

    let currentUserData = JSON.parse(localStorage.getItem("currentUserData") || '{}');
    let currentUserIdentity = JSON.parse(localStorage.getItem("currentUserIdentity") || '""');

    if (Object.keys(currentUserData) != undefined && Object.keys(currentUserData) != null && Object.keys(currentUserData).length > 0) {
      this.service.currentUserDetail = currentUserData;
      if (currentUserIdentity.length > 0) {
        if (currentUserIdentity == "Doctor") {
          this.service.IsDoctorActive = true;
          this.service.activateDoctor = true;
          this.service.doctorDetail = currentUserData;
          this.service.setDetailHeader();
        } else {
          this.service.IsPatientActive = true;
          this.service.activatePatient = true;
          this.service.patientDetail = currentUserData;
          this.service.setDetailHeader();
        }
        this.router.navigate(["/home/detail"]);
      }
    } else {
      this.router.navigate(["/login"]);
    }
  }

  getDepth(outlet) {
    return outlet.activatedRouteData['depth'];
  }

  // prepareRoute(outlet) {
  //   debugger
  //   return outlet && outlet.activatedRouteData && outlet.activatedRouteData['depth'];
  // }
}

