import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/Services/common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public service: CommonService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
  }

  Form: FormGroup = this.fb.group({
    Name: [null, [Validators.required, Validators.minLength(2), Validators.pattern("[a-zA-Z]*")]],
    Qualification: [null, [Validators.required, Validators.minLength(2), Validators.pattern("[a-zA-Z]*")]],
    Email: [null, [Validators.required, Validators.email]],
    PhoneNumber: [null, [Validators.required, Validators.minLength(9)]],
    // ProfileImage: [null, [Validators.required, Validators.minLength(6), Validators.pattern("[a-zA-Z]*")]],
    UserName: [null, [Validators.required, Validators.minLength(4), Validators.pattern("[a-zA-Z]*")]],
    Password: [null, [Validators.required, Validators.minLength(5), Validators.pattern("[0-9]*")]],
    ProfileImage: [""]
  });

  setDataToReactiveForm() {
    debugger
    if (this.service.IsPatientActive) {
      this.Form = this.fb.group({
        Name: [this.name.value, [Validators.required, Validators.minLength(2), Validators.pattern("[a-zA-Z]*")]],
        Email: [this.email.value, [Validators.required, Validators.email]],
        PhoneNumber: [this.phoneNumber.value, [Validators.required, Validators.minLength(6)]],
        UserName: [this.userName.value, [Validators.required, Validators.minLength(4), Validators.pattern("[a-zA-Z]*")]],
        Password: [this.password.value, [Validators.required, Validators.minLength(5), Validators.pattern("[0-9]*")]],
        ProfileImage: [this.Form.get("ProfileImage").value]
      });
    }

  }


  public submitted = false;

  get name() {

    return this.Form.get("Name");
  }

  get qualification() {

    return this.Form.get("Qualification");

  }
  get email() {

    return this.Form.get("Email");

  }

  get phoneNumber() {

    return this.Form.get("PhoneNumber");

  }
  get userName() {

    return this.Form.get("UserName");

  }
  get password() {

    return this.Form.get("Password");

  }


  activeDoctor() {

    this.service.IsPatientActive = false;
    if (!this.service.IsDoctorActive) {
      this.service.IsDoctorActive = true;
    } else this.service.IsDoctorActive = false;
  }

  activePatient() {

    this.service.IsDoctorActive = false;
    if (!this.service.IsPatientActive) {
      this.service.IsPatientActive = true;
    } else this.service.IsPatientActive = false;
  }

  createUser() {
    debugger
    this.submitted = true;
    this.setDataToReactiveForm();
    if (this.Form.invalid) {
      alert("please given correct value to all fields");
      this.Form.reset();
      return;
    }
    if (this.service.IsDoctorActive) {
      this.service.postDoctorData(this.Form.value).subscribe(() => { alert("created successfully"); this.navigateToLogInPage(); });
      this.Form.reset();
    } else {
      this.service.postPatientData(this.Form.value).subscribe(() => { alert("created successfully"); this.navigateToLogInPage(); });
    }


  }

  navigateToLogInPage() {
    window.location.reload();
    this.router.navigate(['/login']);
  }

  uploadFile(event) {
    debugger
    let file = event.target.file[0];
    const formData: FormData = new FormData();
    formData.append("uploadImage", file, file.name);

  }

}
