import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../Services/common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DoctorVM, PatientVM } from 'src/app/Interfaces/Interfaces';
import { Router, Route, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public service: CommonService, private fb: FormBuilder, private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.service.getUrlFromJson().subscribe((x: any) => {
      debugger
      this.service.url = x;
      this.service.getDoctorData().subscribe((x: any) => {
        debugger
        console.log(x);
        this.doctorData = x;
      });
      this.service.getPatientData().subscribe((x: any) => { console.log(x); this.patientData = x; });
    });

  }

  Form: FormGroup = this.fb.group({
    UserName: ["", [Validators.required, Validators.minLength(3), Validators.pattern("[a-zA-Z]*")]],
    Password: ["", [Validators.required, Validators.minLength(4)]]
  });

  public submitted: boolean = false;
  public doctorData: DoctorVM[] = [];
  public patientData: PatientVM[] = [];

  get username() {
    return this.Form.get("UserName");
  }
  get password() {
    return this.Form.get("Password");
  }

  activeDoctor() {

    this.submitted = false;
    this.Form.reset();
    this.service.IsPatientActive = false;
    if (!this.service.IsDoctorActive) {
      this.service.IsDoctorActive = true;
    } else this.service.IsDoctorActive = false;
  }

  activePatient() {

    this.submitted = false;
    this.Form.reset();
    this.service.IsDoctorActive = false;
    if (!this.service.IsPatientActive) {
      this.service.IsPatientActive = true;
    } else this.service.IsPatientActive = false;
  }

  submitData() {

    this.submitted = true;
    if (this.Form.invalid) {
      alert("please given correct value to all fields.");
      this.Form.reset();
      this.submitted = false;
      return;
    }

    if (this.service.IsDoctorActive) {
      if (this.doctorData.length > 0) {
        let userName = this.Form.get("UserName").value;
        let password = this.Form.get("Password").value;

        let checkUserName = this.doctorData.filter((x) => {
          return x.UserName == userName
        });
        if (checkUserName.length > 0) {
          let checkPassword = checkUserName.filter((x) => {
            return x.Password == password
          });
          if (checkPassword.length > 0) {
            let currentUserIdentity = "Doctor";
            localStorage.setItem("currentUserIdentity", JSON.stringify(currentUserIdentity));
            this.service.activateDoctor = true;
            localStorage.setItem("currentUserData", JSON.stringify(checkPassword[0]));
            let currentUserData = JSON.parse(localStorage.getItem("currentUserData" || "{}"));
            if (Object.keys(currentUserData).length > 0) {
              this.service.currentUserDetail = currentUserData;
            }
            alert("log in successfully");
            this.service.getSingleDoctorData(checkPassword[0].RecId).subscribe((x) => {
              this.service.doctorDetail = x;
              this.service.setDetailHeader();
            });
            window.location.reload();
            this.router.navigate(["/home/detail"]);
          } else { alert("password not matching") };

        } else { alert("user not found") };

      } else { alert("Server error") };
      this.Form.reset();
      this.submitted = false;

    } else if (this.service.IsPatientActive) {
      if (this.patientData.length > 0) {
        let userName = this.Form.get("UserName").value;
        let password = this.Form.get("Password").value;

        let checkUserName = this.patientData.filter((x) => {
          return x.UserName == userName
        });
        if (checkUserName.length > 0) {
          let checkPassword = checkUserName.filter((x) => {
            return x.Password == password
          });
          if (checkPassword.length > 0) {
            let currentUserIdentity = "Patient";
            localStorage.setItem("currentUserIdentity", JSON.stringify(currentUserIdentity));
            localStorage.setItem("currentUserData", JSON.stringify(checkPassword[0]));
            let currentUserData = JSON.parse(localStorage.getItem("currentUserData" || "{}"));
            if (Object.keys(currentUserData).length > 0) {
              this.service.currentUserDetail = currentUserData;
            }
            this.service.activatePatient = true;
            alert("log in successfully");
            this.service.getSinglePatientData(checkPassword[0].RecId).subscribe((x) => {
              this.service.patientDetail = x;
              this.service.setDetailHeader();
            });
            // window.location.reload();
            this.router.navigate(["/home/detail"]);
          } else { alert("password not matching") };

        } else { alert("user not found") };

      } else { alert("Server error") };
      this.Form.reset();
      this.submitted = false;
    } else {
      alert("please choose any option");
      this.Form.reset();
    }
  }


}
