import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from "./components/register/register.component";
import { HomeModule } from './modules/home/home.module';

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: LoginComponent, data: { depth: 1, animation: 'isRight' } },
  { path: "register", component: RegisterComponent, data: { depth: 2 } },
  { path: "home", loadChildren: () => import("./modules/home/home.module").then(x => x.HomeModule), data: { depth: 2, animation: 'isLeft' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
