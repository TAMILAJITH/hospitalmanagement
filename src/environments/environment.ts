// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyATVsrwQ9kZQg1oMK6JHcE1MZYXpyQGGXQ",
    authDomain: "hospitalmanagement-cada1.firebaseapp.com",
    projectId: "hospitalmanagement-cada1",
    storageBucket: "hospitalmanagement-cada1.appspot.com",
    messagingSenderId: "52724281211",
    appId: "1:52724281211:web:1e8fb6239057992b5717de"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
